import api.CassandraConnector;
import api.InputExecutor;
import api.StartUpExecutor;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.SimpleLayout;

import java.io.IOException;

public class RecordStudio {

    public static final String LOG_PATH = "log_dir\\log.txt";

    public static void main(String[] args) throws IOException {
        /* configure log4j */
        BasicConfigurator.configure(new FileAppender(new SimpleLayout(), LOG_PATH));

        try ( CassandraConnector connector = new CassandraConnector() ) {
            connector.connect();

            StartUpExecutor startUpExecutor = new StartUpExecutor();
            startUpExecutor.start(connector.getSession());

            InputExecutor executor = new InputExecutor();
            executor.start(connector.getSession());
        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
