package api.entities;

public class Keyspace {
    private String keyspace_name = null;
    private String create_keyspace_query = null;

    public Keyspace(String keyspace_name, String create_keyspace_query){
        if (keyspace_name == null || keyspace_name.isEmpty())
            throw new IllegalStateException("Keyspace cannot be null or empty.");
        if (create_keyspace_query == null || create_keyspace_query.isEmpty())
            throw new IllegalStateException("Keyspace create query cannot be null or empty.");
        this.keyspace_name = keyspace_name;
        this.create_keyspace_query = create_keyspace_query;
    }

    public String getName(){
        return keyspace_name;
    }

    public String getCreateKeyspaceQuery(){
        return create_keyspace_query;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Keyspace keyspace = (Keyspace) o;

        return keyspace_name != null ? keyspace_name.equalsIgnoreCase(keyspace.keyspace_name) : keyspace.keyspace_name == null;

    }

    @Override
    public int hashCode() {
        return keyspace_name != null ? keyspace_name.hashCode() : 0;
    }
}
