package api.entities;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractTable {
    private String keyspace_name = null;

    private List<String> create_table_queries = new ArrayList<>();
    private TableQuery insert_query = null;
    private TableQuery select_query = null;
    private TableQuery delete_query = null;

    public AbstractTable(String keyspace_name) {
        this.keyspace_name = keyspace_name;
    }

    public String getKeyspace() {
        return keyspace_name;
    }

    /* below methods to initialize table */

    protected void setCreateTableQuery (String... create_table_queries){
        Collections.addAll(this.create_table_queries, create_table_queries);
    }

    protected void setInsertQuery(TableQuery insert_query) {
        this.insert_query = insert_query;
    }

    protected void setSelectQuery(TableQuery select_query) {
        this.select_query = select_query;
    }

    protected void setDeleteQuery(TableQuery delete_query) {
        this.delete_query = delete_query;
    }

    /* below methods to interact with table */

    public void create_table(Session session){
        if (create_table_queries.isEmpty())
            throw new IllegalStateException("Create table queries are empty.");
        create_table_queries.forEach(session::execute);
    }

    public abstract void fill_table(Session session);

    public void select_rows(Session session, String... params){
        if (select_query == null)
            throw new IllegalStateException("Select query were`nt initialized.");
        if (select_query.params_length >= params.length) {
            ResultSet set = session.execute(select_query.getQuery(params));
            for (Row row : set){
                System.out.println(row);
            }
        }
        else System.out.println("Select: Not enough arguments.");
    }

    public void insert_row(Session session, String... params){
        if (insert_query == null)
            throw new IllegalStateException("Insert query were`nt initialized.");
        if (insert_query.params_length >= params.length) {
            session.execute(insert_query.getQuery(params));
        }
        else System.out.println("Insert: Not enough arguments.");
    }

    public void delete_row(Session session, String... params) {
        if (delete_query == null)
            throw new IllegalStateException("Delete query were`nt initialized.");
        if (delete_query.params_length >= params.length) {
            session.execute(delete_query.getQuery(params));
        }
        else System.out.println("Delete: Not enough arguments.");
    }

    public abstract void special(Session session, String... params);

    public abstract String getName();

    @Override
    public String toString() {
        return getName();
    }

    protected static class TableQuery {
        private String query;
        private int params_length;

        public TableQuery(String query, int params_length) {
            this.query = query;
            this.params_length = params_length;
        }

        public String getQuery(String... params){
            return String.format(query, params);
        }
    }
}
