package api.entities;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Random;
import java.util.Scanner;

public class GradesTable extends AbstractTable {

    public GradesTable(String keyspace_name) {
        super(keyspace_name);
        setCreateTableQuery(
                "CREATE TABLE " + getKeyspace() + ".Grades (author text, album text, song text, user text, date timestamp, grade text, PRIMARY KEY ((author, album, song), user));",
                "CREATE INDEX ON " + getKeyspace() + ".Grades(grade);");
        setInsertQuery(new TableQuery("INSERT INTO " + getKeyspace() + ".Grades (author, album, song, user, date, grade) " +
                "VALUES ('%s', '%s', '%s', '%s', toTimestamp(now()), '%s');", 5));
        setSelectQuery(new TableQuery("SELECT * FROM " + getKeyspace() + ".Grades WHERE author = '%s' AND album = '%s' AND song = '%s' LIMIT 100;", 3));
        setDeleteQuery(new TableQuery("DELETE FROM " + getKeyspace() + ".Grades WHERE author = '%s' AND album = '%s' AND song = '%s';", 3));
    }

    @Override
    public String getName() {
        return "Grades";
    }

    @Override
    public void special(Session session, String... params) {
        if (params.length < 3) {
            System.out.println("Not enough arguments for special.");
            return;
        }
        System.out.println("Distribution of grades:");
        for (int i = 1; i <= 5; i++) {
            String query = String.format("SELECT COUNT(*) as coun FROM " + getKeyspace()
                    + ".Grades where author = '%s' and album = '%s' and song = '%s' and grade = '" + i + "';", params);
            ResultSet result = session.execute(query);
            System.out.println(i + ": " + result.one().getLong("coun"));
        }
    }

    @Override
    public void fill_table(Session session) {
        final String path = "fill_tables/GradesTable";
        URL url = this.getClass().getClassLoader().getResource(path);
        File folder;
        try {
            folder = new File(url.toURI());
        } catch (URISyntaxException e) {
            folder = new File(url.getPath());
        } catch (NullPointerException e) {
            System.out.println("Bad path in fill_table.");
            return;
        }
        File[] authors_folder = folder.listFiles();
        for (File author_file : authors_folder) {
            File[] albums_files = author_file.listFiles();
            for (File album_file : albums_files) {
                Random random = new Random();
                try (Scanner sc = new Scanner(album_file)) {
                    while (sc.hasNext()) {
                        final String user_name = "user";
                        final String author = author_file.getName();
                        final String album = album_file.getName();
                        final String song = sc.nextLine();
                        final StringBuilder user = new StringBuilder(user_name);
                        final int quantityOfStringsPerRow = 20_000;
                        long start = System.currentTimeMillis();
                        for (int i = 0; i < quantityOfStringsPerRow; i++) {
                            int grade = 1 + random.nextInt(5);
                            user.delete(user_name.length(), user.length());
                            user.append(i);
                            insert_row(session, author, album, song, user.toString(), Integer.toString(grade));
                        }
                        long end = System.currentTimeMillis();
                        System.out.println(quantityOfStringsPerRow + " rows inserted for " + ((end - start) / 1000.0f));
                    }
                } catch (FileNotFoundException e) {
                    System.out.println("Scanner for file cant be opened.");
                }
            }
        }
    }
}
