package api;

import api.entities.AbstractTable;
import api.entities.DatabaseSingleton;
import com.datastax.driver.core.*;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class StartUpExecutor {

    public void start(Session session) {
        DatabaseSingleton database = DatabaseSingleton.getInstance();
        database.getKeyspaces().forEach(keyspace -> {
            if (!keyspaceExists(session, keyspace.getName())) {
                System.out.println("Keyspace " + keyspace + " would be created automatically.");
                session.execute(keyspace.getCreateKeyspaceQuery());
            }
            List<AbstractTable> tables = database.getTables(keyspace);
            tables.forEach(table -> {
                if (!tableExists(session, keyspace.getName(), table.getName())) {
                    System.out.println("Table " + keyspace + "." + table + " would be created automatically.");
                    table.create_table(session);
                }
                if (tableEmpty(session, keyspace.getName(), table.getName())) {
                    System.out.println(keyspace + "." + table + " is empty: now it would be filled up automatically.");
                    table.fill_table(session);
                }
            });
        });
    }

    public boolean tableEmpty(Session session, String keyspace, String table) {
        ResultSet result = session.execute("SELECT * FROM " + keyspace + "." + table + " LIMIT 1;");
        return result.one() == null;
    }

    public boolean tableExists(Session session, String keyspace, String table) {
        KeyspaceMetadata keyspaceMetadata = session.getCluster().getMetadata().getKeyspace(keyspace);
        TableMetadata tableMetadata = keyspaceMetadata.getTable(table);
        return tableMetadata != null;
    }

    public boolean keyspaceExists(Session session, String keyspace) {
        KeyspaceMetadata metadata = session.getCluster().getMetadata().getKeyspace(keyspace);
        return metadata != null;
    }

    @Deprecated
    public String getQueryFromFile(String filepath) {
        StringBuilder result = new StringBuilder();
        ClassLoader loader = getClass().getClassLoader();
        try {
            File file = new File(loader.getResource(filepath).getFile());
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    result.append(scanner.nextLine());
                }
            }
            return result.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
